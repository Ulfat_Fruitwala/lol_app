// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        // Search bar input
        search: "",

        // List of recipes
        recipes: [],

        // Toggle helper
        add_mode: false,
        see_mode: false,

        // Below are variables used whilst adding a recipe
        add_name: "",
        add_ingredients: "",
        add_method: "",
        add_time: "",
        add_img: "",

        // Variables used whilst adding an image to a recipe
        selection_done: false,

        // Below are variables used whilst enlarging a recipe
        see_name: "",
        see_ingredients: "",
        see_method: "",
        see_time: "",
        see_full_time: "",
        see_idx: -1,
        see_id: -1,
        see_state: {},
    };
    // File
    app.file = null;

    app.select_file = function(event){
        let input = event.target;
        app.file = input.files[0];
        if(app.file){
            app.vue.selection_done = true;
            // primitive method from browser
            let reader = new FileReader();
            reader.addEventListener("load", function(){
                app.vue.add_img = reader.result;
            });
            reader.readAsDataURL(app.file);
        }
        app.file = null;
    };

    app.add_recipe = function(){
        axios.post(add_recipe_url,
            {
                name: app.vue.add_name,
                ingredients: app.vue.add_ingredients,
                method: app.vue.add_method,
                time: app.vue.add_time,
                img: app.vue.add_img,
            }).then(function(response) {
            app.vue.recipes.push({
                id: response.data.id,
                name: app.vue.add_name,
                ingredients: app.vue.add_ingredients,
                method: app.vue.add_method,
                time_required: app.vue.add_time,
                image: app.vue.add_img,
                _state: {name: 'clean',
                         ingredients: 'clean',
                         method: 'clean',
                         time_required: 'clean' },
            });
            app.enumerate(app.vue.recipes);
            app.reset_form();
            app.set_add_mode(false);
        });
    };

    app.delete_recipe = function(recipe_idx){
        let id = app.vue.recipes[recipe_idx].id;
        axios.get(delete_recipe_url, {params: {id: id}}).then(function (response){
            for(let i = 0; i < app.vue.recipes.length; i++){
                if(app.vue.recipes[i].id === id){
                    app.vue.recipes.splice(i, 1);
                    app.enumerate(app.vue.recipes);
                    break;
                }
            }
        });
        app.set_see_mode(false);
    };

    app.set_add_mode = function(new_status){
        app.reset_form();

        if(new_status == true){
            if(app.vue.see_mode == true)
                app.vue.see_mode = false;
                app.reset_see_vars();
        }
        app.vue.add_mode = new_status;
    };

    app.set_see_mode = function(new_status){
        if(new_status == true){
            if(app.vue.add_mode == true)
                app.reset_form();
                app.vue.add_mode = false;
        }
        app.vue.see_mode = new_status;
    };

    app.see_recipe = function(recipe_idx){
        // Set the 'see' variables
        let r = app.vue.recipes[recipe_idx];
        app.vue.see_name = r.name;
        app.vue.see_ingredients = r.ingredients;
        app.vue.see_method = r.method;
        app.vue.see_time = r.time_required;
        app.vue.see_idx = r._idx;
        app.vue.see_id = r.id;
        app.vue.see_state = r._state;
        // Set visible
        app.set_see_mode(true);
        window.scrollTo(0,0);
    };

    app.start_edit = function(field_val){
        switch(field_val){
            //edit the title
            case 0:
                app.vue.see_state['name'] = 'edit';
                break;
            // edit ingredients
            case 1:
                app.vue.see_state['ingredients'] = 'edit';
                break;
            // edit methods
            case 2:
                app.vue.see_state['method'] = 'edit';
                break;
            // edit time required
            case 3:
                app.vue.see_state['time_required'] = 'edit';
                break;
        }
    };

    app.stop_edit = function(field_val){
        let r = app.vue.recipes[app.vue.see_idx];
        switch(field_val){
            //edit the title
            case 0:
                if(app.vue.see_state['name'] === 'edit')
                app.vue.see_state['name'] = 'pending';
                axios.post(edit_recipe_url,
                {
                    id: app.vue.see_id,
                    value: app.vue.see_name,
                    fn: 'name',
                }).then(function (result) {
                    app.vue.see_state['name'] = 'clean';
                    r.name = app.vue.see_name;
                });
                break;
            // edit ingredients
            case 1:
                if(app.vue.see_state['ingredients'] === 'edit')
                app.vue.see_state['ingredients'] = 'pending';
                axios.post(edit_recipe_url,
                {
                    id: app.vue.see_id,
                    value: app.vue.see_ingredients,
                    fn: 'ingredients',
                }).then(function (result) {
                    app.vue.see_state['ingredients'] = 'clean';
                    r.ingredients = app.vue.see_ingredients;
                });
                break;
            // edit methods
            case 2:
                if(app.vue.see_state['method'] === 'edit')
                app.vue.see_state['method'] = 'pending';
                axios.post(edit_recipe_url,
                {
                    id: app.vue.see_id,
                    value: app.vue.see_method,
                    fn: 'method',
                }).then(function (result) {
                    app.vue.see_state['method'] = 'clean';
                    r.method = app.vue.see_method;
                });
                break;
            // edit time required
            case 3:
                if(app.vue.see_state['time_required'] === 'edit')
                app.vue.see_state['time_required'] = 'pending';
                axios.post(edit_recipe_url,
                {
                    id: app.vue.see_id,
                    value: app.vue.see_time,
                    fn: 'time_required',
                }).then(function (result) {
                    app.vue.see_state['time_required'] = 'clean';
                    r.time_required = app.vue.see_time;
                });
                break;
        }
    };

    // reindexing recipes
    app.enumerate = (a) => {
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    // This decorates the recipes that come from the server
    // adding information on their status
    // - clean : read-only, the value is saved on the server
    // - edit : the value is being edited
    // - pending: a save is pending
    app.decorate = (a) => {
        a.map((e) => {e._state = {
            name: 'clean',
            ingredients: 'clean',
            method: 'clean',
            time_required: 'clean'
        };});
        return a;
    };

    // clearing out form fields
    app.reset_form = function(){
        app.vue.add_name = "";
        app.vue.add_ingredients = "";
        app.vue.add_method = "";
        app.vue.add_time = "";

        //reset uploading variables
        app.file = null;
        app.vue.selection_done = false;
        app.vue.add_img = "";
    };

    app.reset_see_vars = function(){
        app.vue.see_name = "";
        app.vue.see_ingredients = "";
        app.vue.see_method = "";
        app.vue.see_time = "";
        app.vue.see_idx = -1;
        app.vue.see_state = {};
    };

    // We form the dictionary of all methods, so we can assign them
    // to the Vue app in a single blow.
    app.methods = {
        add_recipe: app.add_recipe,
        delete_recipe: app.delete_recipe,
        set_add_mode: app.set_add_mode,
        set_see_mode: app.set_see_mode,
        see_recipe: app.see_recipe,
        start_edit: app.start_edit,
        stop_edit: app.stop_edit,
        select_file: app.select_file,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        axios.get(load_recipes_url).then(function (response){
            app.vue.recipes = app.decorate(app.enumerate(response.data.recipes));
        });
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code i
init(app);