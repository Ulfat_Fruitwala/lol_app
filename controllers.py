"""
This file defines actions, i.e. functions the URLs are mapped into
The @action(path) decorator exposed the function at URL:

    http://127.0.0.1:8000/{app_name}/{path}

If app_name == '_default' then simply

    http://127.0.0.1:8000/{path}

If path == 'index' it can be omitted:

    http://127.0.0.1:8000/

The path follows the bottlepy syntax.

@action.uses('generic.html')  indicates that the action uses the generic.html template
@action.uses(session)         indicates that the action uses the session
@action.uses(db)              indicates that the action uses the db
@action.uses(T)               indicates that the action uses the i18n & pluralization
@action.uses(auth.user)       indicates that the action requires a logged in user
@action.uses(auth)            indicates that the action requires the auth object

session, db, T, auth, and tempates are examples of Fixtures.
Warning: Fixtures MUST be declared with @action.uses({fixtures}) else your app will result in undefined behavior
"""
import time
from py4web import action, request, abort, redirect, URL
from yatl.helpers import A
from .common import db, session, T, cache, auth, logger, authenticated, unauthenticated, flash
from py4web.utils.url_signer import URLSigner
from py4web.utils.form import Form, FormStyleBulma
from .models import get_user_email
from .settings import APP_FOLDER
import json
import os

url_signer = URLSigner(session)


@action('index')
@action.uses(db, auth, 'index.html')
def index():
    return dict(
        #This is the signed URL for the callback
        load_recipes_url = URL('load_recipes', signer=url_signer),
        add_recipe_url = URL('add_recipe', signer=url_signer),
        delete_recipe_url = URL('delete_recipe', signer=url_signer),
        edit_recipe_url = URL('edit_recipe', signer=url_signer),
        upload_file_url = URL('upload_file', signer=url_signer),
    )

# This is a API function
@action('load_recipes')
@action.uses(url_signer.verify(), db)
def load_recipes():
    recipes = db(db.recipes).select().as_list()
    return dict(recipes=recipes)

# Adding recipe API function
@action('add_recipe', method="POST")
@action.uses(url_signer.verify(), db)
def add_recipe():
    # Inserting into the db
    id = db.recipes.insert(
        name=request.json.get('name'),
        ingredients=request.json.get('ingredients'),
        method=request.json.get('method'),
        time_required=request.json.get('time'),
        image=request.json.get('img')
    )
    return dict(id=id)

# Editing a recipe
@action('edit_recipe', method="POST")
@action.uses(url_signer.verify(), db)
def edit_recipe():
    #update the db
    id = request.json.get("id")
    value = request.json.get("value")
    fn = request.json.get("fn")
    db(db.recipes.id == id).update(**{fn:value})

    return "ok"

# Deleting recipe API function
@action('delete_recipe')
@action.uses(url_signer.verify(), db)
def delete_recipe():
    id = request.params.get('id')
    assert id is not None
    db(db.recipes.id == id).delete()
    return "ok"

# Uploading a file using PUT method (ideal for sending files)
# Not used ATM
@action('upload_file', method="PUT")
@action.uses()
def upload_file():
    file_name = request.params.get("file_name")
    file_type = request.params.get("file_type")
    uploaded_file = request.body
    # Diagnostics
    print("Uploaded: ", file_name, " Type: ", file_type)

    return "ok"